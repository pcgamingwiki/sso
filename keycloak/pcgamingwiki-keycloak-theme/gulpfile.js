const { src, dest, parallel, watch } = require('gulp');
const sass = require('gulp-sass');

function loginCSS() {
    return src('src/css/login.scss')
        .pipe(sass())
        .pipe(dest('build/login/resources/css'))
}

function resources() {
    return src('src/resources/**/*')
        .pipe(dest('build/'))
}

allStyles = parallel(loginCSS)

exports.css = allStyles;
exports.default = parallel(allStyles, resources);
exports.watch = function() {
  watch('src/css/*', allStyles);
  watch('src/resources/**/*', resources);
};