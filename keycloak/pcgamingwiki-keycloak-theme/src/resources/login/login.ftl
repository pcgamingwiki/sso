<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo displayWide=(realm.password && social.providers??); section>
    <#if section = "header">
        ${msg("doLogIn")}
    <#elseif section = "form">
        <div>
            <div class="form">
                <form onsubmit="login.disabled = true; return true;" action="${url.loginAction}"
                      method="post">
                    <#if usernameEditDisabled??>
                        <input id="username" name="username" type='text' tabindex="1"
                               placeholder='<#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if>'
                               disabled/>
                    <#else>
                        <input id="username" name="username" type='text' tabindex="1"
                               placeholder='<#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if>'
                               autofocus autocomplete="off"/>
                    </#if>
                    <input id="password" name="password" type='password' tabindex="2" placeholder='${msg("password")}'/>
                    <div class='splitbox'>
                        <label>
                            <#if realm.rememberMe && !usernameEditDisabled??>
                                <#if login.rememberMe??>
                                    <input tabindex="3" id="rememberMe" name="rememberMe" type="checkbox"
                                           checked> ${msg("rememberMe")}
                                <#else>
                                    <input tabindex="3" id="rememberMe" name="rememberMe"
                                           type="checkbox"> ${msg("rememberMe")}
                                </#if>
                            </#if>
                        </label>
                        <div>
                            <a tabindex="5" href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a>
                        </div>
                    </div>
                    <input name="login" type="submit" tabindex="4" value="${msg("doLogIn")}"/>
                </form>
            </div>
            <#if realm.password && social.providers??>
                <div class="social-providers">
                    <ul class="social-providers-list">
                        <#list social.providers as p>
                            <li class="social-provider">
                                <a class="social-provider-${p.alias}" href="${p.loginUrl}">
                                    <img src="${url.resourcesPath}/img/${p.alias}.svg"/><span>${msg("doLogInWith" + p.displayName)}</span>
                                </a>
                            </li>
                        </#list>
                    </ul>
                </div>
            </#if>
        </div>
    </#if>

</@layout.registrationLayout>
