<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true displayWide=false>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" class="${properties.kcHtmlClass!}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <#if properties.meta?has_content>
            <#list properties.meta?split(' ') as meta>
                <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
            </#list>
        </#if>
        <title>${msg("loginTitle",(realm.displayName!''))}</title>
        <link rel="icon" href="https://static.pcgamingwiki.com/favicons/pcgamingwiki.png"/>
        <#if properties.styles?has_content>
            <#list properties.styles?split(' ') as style>
                <link href="${url.resourcesPath}/${style}" rel="stylesheet"/>
            </#list>
        </#if>
        <#if properties.scripts?has_content>
            <#list properties.scripts?split(' ') as script>
                <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
            </#list>
        </#if>
        <#if scripts??>
            <#list scripts as script>
                <script src="${script}" type="text/javascript"></script>
            </#list>
        </#if>
    </head>

    <body>
    <div class="container">
        <div class="branding">
            <div class="logo">
                <a href="https://www.pcgamingwiki.com/">
                    <img class="mobile" src="${url.resourcesPath}/img//pcgamingwiki-wide.svg"/>
                </a>
                <a href="https://www.pcgamingwiki.com/">
                    <img class="desktop" src="${url.resourcesPath}/img//pcgamingwiki.svg"/>
                </a>
            </div>
            <h1>PCGW Account</h1>
        </div>
        <div class="content">
            <div class="content-wrapper">
                <div class='page-title'>
                    <h1><#nested "header"></h1>
                </div>
                <div class='page-content'>
                    <#if displayMessage && message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
                        <div class="alert alert-${message.type}">
                            <#if message.type = 'success'><span class="${properties.kcFeedbackSuccessIcon!}"></span></#if>
                            <#if message.type = 'warning'><span class="${properties.kcFeedbackWarningIcon!}"></span></#if>
                            <#if message.type = 'error'><span class="${properties.kcFeedbackErrorIcon!}"></span></#if>
                            <#if message.type = 'info'><span class="${properties.kcFeedbackInfoIcon!}"></span></#if>
                            <span class="alert-feedback-text">${kcSanitize(message.summary)?no_esc}</span>
                        </div>
                    </#if>
                    <#nested "form">
                </div>
                <div class='page-post'>
                    <div class='page-post-item'>
                        <h2>New account</h2>
                        <p>
                            <a href="${url.registrationUrl}">Register</a> to create a new <span
                                    style="font-weight:bold">PCGW Account</span>.
                        </p>
                        <p>
                            Use the same email address as registered on the Wiki and/or Forum to migrate your old
                            account.
                        </p>
                    </div>
                    <div class='page-post-item'>
                        <h2>Account benefits</h2>
                        <p>
                            <span style="font-weight:bold">PCGW Account</span> enables ad-free browsing, and
                            provides access to:
                        </p>
                        <ul>
                            <li><a href="https://www.pcgamingwiki.com/wiki/Home">Wiki</a></li>
                            <li><a href="https://community.pcgamingwiki.com/">Community</a> (Forum and Files)</li>
                            <li>Sidewikis</li>
                        </ul>
                        <p>
                            Manage your account from the <a href="https://sso.pcgamingwiki.com/auth/realms/PCGamingWiki/account">Account Management</a> page.
                        </p>
                    </div>
                    <div class='page-post-item'>
                        <h2>Need help?</h2>
                        <p>
                            Please take a look at common account questions in our <a href="https://pcgamingwiki.com/wiki/PCGamingWiki:Account">FAQ</a>
                        </p>
                        <p>
                            Email <a href="mailto:help@pcgamingwiki.com">help@pcgamingwiki.com</a> for issues with your PCGW Account.
                        </p>
                        <p>
                            Join us on our <a href="https://pcgamingwiki.com/wiki/PCGamingWiki:Discord">Discord</a> server and ask for help in the <span style="font-weight:bold">#support</span> channel.
                        </p>
                        <p>
                            <a href="https://pcgamingwiki.com/wiki/PCGamingWiki:Privacy_policy">Privacy policy</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>

    </html>
</#macro>
